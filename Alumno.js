function Alumno(i, nc, n, ap, am, g){
	var _id = i;
	var _numero_de_control = nc;
	var _nombre = n;
	var _apellido_paterno = ap;
	var _apellido_materno = am;
	var _genero = g;
	var _calificaciones = [];

	function _getId(){
		return _id;
	}
	function _getNumControl(){
		return _numero_de_control;
	}
	function _getNombre(){
    	return _nombre;
  }
  function _getApellidoPaterno(){
    	return _apellido_paterno;
  }
 	function _getApellidoMaterno(){
  	  return _apellido_materno;
  	}
  function _getGenero(){
    	return _genero;
  }
  function _registrar_calificacion(calificacion){
    _calificaciones.push(calificacion);
  }

  function _imprimir_calificaciones(){
    for(var i = 0; i < _calificaciones.length; i++){
      console.log(_calificaciones[i].materia.get_nombre() + ": " + _calificaciones[i].calificacion);
    }
  }

  	return{
  		"get_numero_de_control": _getNumControl,
  		"get_id": _getId,
  		"get_nombre": _getNombre,
  		"get_apellido_paterno": _getApellidoPaterno,
  		"get_apellido_materno": _getApellidoMaterno,
  		"get_genero": _getGenero,
      "registrar_calificacion": _registrar_calificacion,
      "imprimir_calificaciones": _imprimir_calificaciones

  	}
}