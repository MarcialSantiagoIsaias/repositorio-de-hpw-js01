function Grupo(i, c, n){
	var _id = i;
	var _clave = c;
	var _nombre = n;
	var _alumnos = [];
	var _docentes = [];
	var _materias = [];
	var _calificaciones = [];

	function _getId(){
		return _id;
	}

	function _getClave(){
		return _clave;
	}

	function _getNombre(){
		return _nombre;
	}
	function _agregar_materia(materia){
		if(_materias.length < 7){
			_materias.push(materia);
		}
	}
	function _agregar_docente_a_materia(docente, materia){
     var existe_materia = false;
     var docente_asignado_en_otro_grupo = false;
	   
     for (var i = 0; i < _materias.length; i++) {
      if(_materias[i] === materia){
        existe_materia = true;
        break;
      }
     }

     for(var i = 0; i < _docentes; i++){
      if(_docentes[i] === docente){
        docente_asignado_en_otro_grupo = true;
        break;
      }
     }
     if(existe_materia && !docente_asignado_en_otro_grupo){
      _docentes.push(docente);
		  docente.agrega_materia(materia);
    }else{
      console.log("La materia no se encuentra registrada o el docente ya esta asignado en otro grupo");
    }
	}

  function _registrar_calificacion_de_alumno_en_materia(alu, mat, cal){
    if( cal >= 0 && cal <= 10 ){
      var cali = {
        alumno: alu,
        materia: mat,
        calificacion: cal
      };
      _calificaciones.push(cali);
      alu.registrar_calificacion(cali);
    }else{
      console.log("Calificacion inválida");
    }

  }
	function _agregar_al(alumno){
    if(!_existeAlumno(alumno.get_numero_de_control())){
    	if(_alumnos.length < 30){
    		_alumnos.push(alumno);  
    	}
      	else{
      		console.log("Se ha alcanzado el cupo máximo")
      	}
     }else{
     console.log("Ya existe un alumno con ese número de control");
     }
  }
  
  function _buscar_al_alumno(nc){
    for(var i = 0; i<_alumnos.length; i++){
      if(_alumnos[i].get_numero_de_control() === nc){
        return i;
      }
    }
  }
  
  function _existeAlumno(nc){
    for(var i = 0; i<_alumnos.length; i++){
      if(_alumnos[i].get_numero_de_control() === nc){
        return true;
      }else{
        return false;
      }
    }
  }
  
  function _quitarAlumno(nc){
    var indice = _buscar_al_alumno(nc);
    if(indice >= 0){
      _alumnos.splice(indice,1);
    }else{
      console.log("No existe el alumno");
    }
  }
  
  function _listarAlumnos(){
    for(var i = 0; i<_alumnos.length; i++){
      console.log(_alumnos[i].get_numero_de_control() + ". " + _alumnos[i].get_nombre() + " " + _alumnos[i].get_apellido_paterno() + " " + _alumnos[i].get_apellido_materno());
    }
  }
  
  function _actualizarAlumno(nc, n, ap, am, g){
    var indice = _buscarAlumno(nc);
    _alumnos[indice].setNombre(n);
    _alumnos[indice].setApPaterno(ap);
    _alumnos[indice].setApMaterno(am);
    _alumnos[indice].setGenero(g);
           
  }

  function _mostrar_resumen(){
    console.log("Grupo: " + _nombre);
    console.log("Cantidad de alumnos: " + _alumnos.length);
    console.log("Materias registradas: " + _materias.length);
    console.log("Docentes registrados: " + _docentes.length);
  }
  return{
    "get_clave": _getClave,
    "get_nombre": _getNombre,
    "listar_alumnos": _listarAlumnos,
    "agregar_alumno": _agregar_al,
    "quitar_alumno": _quitarAlumno,
    "buscar_alumno": _buscar_al_alumno,
    "actualizar_alumno": _actualizarAlumno,
    "agregar_docente_a_materia": _agregar_docente_a_materia,
    "agregar_materia": _agregar_materia,
    "mostrar_resumen": _mostrar_resumen,
    "registrar_calificacion_de_alumno_en_materia": _registrar_calificacion_de_alumno_en_materia
  }
}