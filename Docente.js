function Docente(i, r, n, ap, am, g){
	var _id = i;
	var _rfc = r;
	var _nombre = n;
	var _apellido_paterno = ap;
	var _apellido_materno = am;
	var _genero = g;
	var _grupos = [];
	var _materias = [];

	function _getId(){
		return _id;
	}

	function _getRfc(){
		return _rfc;
	}

	function _getNombre(){
		return _nombre;
	}

	function _getApellidoPaterno(){
		return _apellido_paterno;
	}

	function _getApellidoMaterno(){
		return _apellido_materno;
	}

	function _getGenero(){
		return _genero;
	}

	function _agrega_materia(materia){
		_materias.push(materia);
	}

	function _get_materias(){
		return _materias;
	}
	return{
		"get_id": _getId,
		"get_rfc": _getRfc,
		"get_nombre": _getNombre,
		"get_apellido_materno": _getApellidoMaterno,
		"get_apellido_paterno": _getApellidoPaterno,
		"get_genero": _getGenero,
		"agrega_materia": _agrega_materia,
		"get_materias": _get_materias

	}
}