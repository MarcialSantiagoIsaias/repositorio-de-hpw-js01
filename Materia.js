function Materia(i, c, n){
	var _id = i;
	var _clave = c;
	var _nombre = n;

	function _getId(){
		return _id;
	}

	function _getClave(){
		return _clave;
	}

	function _getNombre(){
		return _nombre;
	}

	return{
		"get_id": _getId,
		"get_clave": _getClave,
		"get_nombre": _getNombre
	}
}